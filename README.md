This demo tool will collect files from a folder in HCP Anywhere (ideally, a Team Folder),
transport them into a Bucket in HCP (using an S3 request) and then delete the files from
HCP Anywhere.

The configuration file (hcpaw2s3.json) needs to be edited with the access details to both
HCP Anywhere and HCPto function.


For a simple run:

* Find a computer that is able to connect to both HCP Anywhere and HCP
* Make sure you have Python 3.8 (or newer) installed on that computer
* Clone the repository
* Change into the hcpaw2s3 folder
* Create a Python virtual environment
  * $ pip install -m venv .venv
  * $ source .venv/bin/activate (Linux/MacOS)
    or
    > .venv\Scripts\activate.bat (Windows)
* Install dependencies
  * (.venv) $ pip install -r pip-requirememts.txt
* Run:
  * (.venv) $ python src/hcpaw2s3.py hcpaw2s3.json

It will now ask you to create a config file (hcpaw2s3.json). Allow that, edit it to your needs and run the tool again.

