# The MIT License (MIT)
#
# Copyright (c) 2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import logging
from time import sleep
from os import unlink
import boto3
import botocore
from boto3.s3.transfer import S3Transfer, TransferConfig
from botocore.client import Config
from botocore.utils import fix_s3_host
import click


class S3Con:
    """
    Deal with S3.
    """

    def __init__(self, endpoint='', bucket='', path='', accesskey='', securekey=''):
        """
        :param endpoint:   the S3 service endpoint
        :param bucket:     the bucket
        :param path:       target path
        :param accesskey:  the access key
        :param securekey:  the security key
        """
        self.log = logging.getLogger(__name__)
        self.endpoint = endpoint
        self.bucket = bucket
        self.path = path
        self.accesskey = accesskey
        self.securekey = securekey
        self._config = None

    def storeobject(self, file, path):
        """
        Store a file as an object in an S3 bucket.

        :param file:  the file to store
        :param path:  the target path in the S3 bucket
        """
        title = f'Storing file {path} to S3 service'
        print(title)
        print('='*len(title))

        # setup a Config object
        self._config = Config(s3={'addressing_style': 'path',
                                  'payload_signing_enabled': True},
                              signature_version='s3v4')

        cl = boto3.client('s3',
                          aws_access_key_id=self.accesskey,
                          aws_secret_access_key=self.securekey,
                          endpoint_url=self.endpoint,
                          verify=False,
                          config=self._config)
        cl.meta.events.unregister('before-sign.s3', fix_s3_host)

        try:
            transconf = TransferConfig(multipart_threshold=8*1024**2,
                                       max_concurrency=10,
                                       multipart_chunksize=8*1024**2
                                       )
            transfer = S3Transfer(client=cl, config=transconf)

            transfer.upload_file(file, self.bucket, self.path+'/'+path[1:],
                                 # extra_args={'Metadata': para.meta,}
                                 )
        except Exception:
            raise
        finally:
            unlink(file)  # remove temporary file

        print()
        sleep(1.0)  # wait a sec to allow for logging output to happen
        try:
            click.confirm("Continue?", default=True, abort=True)
        except click.exceptions.Abort:
            sys.exit('Canceled')
        print()
        print()
