# The MIT License (MIT)
#
# Copyright (c) 2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
from json import load

from hcpaw2s3lib import parseargs
from hcpaw2s3lib.conf import loadconfig
from hcpaw2s3lib.awcon import AwCon
from hcpaw2s3lib.s3con import S3Con
from hcpaw2s3lib.logging import setupboto3logging

import click
from pprint import pprint


def main():
    opts = parseargs()
    cnf = loadconfig(opts.config)                          # load the configuration from a json file

    setupboto3logging()                                    # set up logging for Boto3

    awcon = AwCon(uri=cnf['hcp anywhere']['URI'],          # set up a connection to HCP Anywhere
                  user=cnf['hcp anywhere']['USER'],
                  password=cnf['hcp anywhere']['PASSWORD'])
    awcon.connect()                                        # connect to HCP Anywhere
    files = {x: [] for x in cnf['hcp anywhere']['PATHS']}  # create an empty dict to collect found files

    s3con = S3Con(endpoint=cnf['s3']['ENDPOINT'],          # set up a connection to an S3 service
                  bucket=cnf['s3']['BUCKET'],
                  path=cnf['s3']['PATH'],
                  accesskey=cnf['s3']['ACCESSKEY'],
                  securekey=cnf['s3']['SECUREKEY'])

    for path in cnf['hcp anywhere']['PATHS']:              # check paths for new files
        files[path] = awcon.listfolder(path)

    for folder in files:                                   # handle folders
        for file in files[folder]:

            tf = awcon.getfile(file['path'])               # read the content into a temporary file
            s3con.storeobject(tf, file['path'])            # send the file to S3 service
            awcon.deletefile(file['path'], file['etag'])   # delete from HCP Anywhere

    awcon.close()

    print('Finished successfully.')
