# The MIT License (MIT)
#
# Copyright (c) 2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import binascii
import sys
from base64 import b64decode
from tempfile import NamedTemporaryFile
from urllib.parse import quote, urlparse
import requests
import click
from requests.packages.urllib3 import disable_warnings

import time
from collections import OrderedDict
from json import dumps, loads
from pprint import pprint, pformat


class AwCon:
    """
    Deal with HCP Anywhere.
    """
    def __init__(self, uri, user, password):
        """
        :param uri:       HCP Anywhere's URI
        :param user:      the HCP Anywhere user
        :param password:  its password
        """
        self.uri = uri
        self.user = user
        self.password = password
        self.__preconnect()
        self.api = ''
        self.expires = ''

        disable_warnings()  # prevent from warning for unverified TLS session

    def __preconnect(self):
        """
        Set up the session environment.
        """

        self.session = requests.Session()
        self.session.verify = False
        self.session.headers.update({'Accept': 'application/json',
                                     'Content-Type': 'application/json'})

    def connect(self):
        """
        Connect to HCP Anywhere and gain an access token.
        """
        # this is for AD authentication
        self.session.headers.update({'X-HCPAW-FSS-API-VERSION': '2.1.1'})

        try:
            auth = OrderedDict([('username', self.user),
                                ('password', b64decode(self.password).decode()),
                                ('grant_type', 'urn:hds:oauth:negotiate-client')])
        except binascii.Error:
            sys.exit('Fatal: HCP Anywhere password seems not to be base64 encoded?')

        try:
            r = self.session.post(self.uri + '/fss/public/login/oauth',
                                  data=dumps(auth))
        except requests.exceptions.ConnectionError as e:
            print('Connection to {} failed...'.format(self.aw),
                   'hint: {}'.format(e).format(self.aw, e))
        else:
            if r.status_code == 200:
                self.__showrequestandresponse(r, 'Gain an authentication token')

                rr = r.json()
                self.api = sorted(r.headers['X-HCPAW-SUPPORTED-FSS-API-VERSIONS'].split(','), reverse=True)[0]
                self.session.headers.update({'X-HCPAW-FSS-API-VERSION': self.api,
                                             'Authorization': f'{rr["token_type"]} {rr["access_token"]}'
                                             })
                self.expires = time.strftime('%Y/%m/%d %H:%M:%S',
                                             time.localtime(time.time() +
                                                            rr['expires_in']))
                # self.logger.debug('acquire_token returned: {} {} {}'
                #                   .format(r.status_code, r.reason, r.elapsed))
            else:
                sys.exit(f'Fatal: Login incorrect ({r.status_code} {r.reason})')
        finally:
            del auth

    def listfolder(self, path):
        """
        List a shared folder.

        :param path:  the folder's path
        :returns:     a list of paths
        """
        body = {'path': path,
                'pageSize': 100,
                'sortingCriterion': 'MODIFY_DATE',
                'sortingDirection': 'DESC'}
        paths = []

        try:
            r = self.session.post(self.uri + '/fss/public/folder/entries/list',
                                  data=dumps(body))
        except requests.exceptions.ConnectionError as e:
            print('Connection to {} failed...'.format(self.aw),
                   'hint: {}'.format(e).format(self.aw, e))
        else:
            if r.status_code == 200:
                self.__showrequestandresponse(r, f'CHECK TEAM FOLDER CONTENT ({path})')
                rr = r.json()
                for entry in rr['entries']:
                    if entry['parent'] == path:
                        paths.append({'path': '/'.join([path, entry['name']]),
                                     'etag': entry['etag']})
                return paths
            else:
                sys.exit(f'Fatal: request failed ({r.status_code} {r.reason})')

    def getfile(self, path):
        """
        Download a file from HCP Anywhere, store in tempfile.

        :param path:  the file to download
        :return:      a file handle to an invisible tempfile
        """
        tf = NamedTemporaryFile('w+b', delete=False)

        body = {'path': path}

        try:
            r = self.session.post(self.uri + '/fss/public/file/stream/read',
                                  data=dumps(body),
                                  headers={'Accept': 'application/octet-stream'})
        except requests.exceptions.ConnectionError as e:
            sys.exit(f'Connection to {self.aw} failed - hint: {e}')
        else:
            if r.status_code == 200:
                self.__showrequestandresponse(r, f'GET FILE {path}', showbody=False)
                tf.write(r.content)
                tfname = tf.name
                tf.close()
                return tfname
            else:
                sys.exit(f'Fatal: request failed ({r.status_code} {r.reason})')

    def deletefile(self, path, etag):
        """
        Deltee a file from HCP Anywhere.

        :param path:  the file to delete
        :param etag:  its etag
        """
        body = {'path': path,
                'etag': etag}

        try:
            r = self.session.post(self.uri + '/fss/public/path/delete',
                                  data=dumps(body))
        except requests.exceptions.ConnectionError as e:
            print('Connection to {} failed...'.format(self.aw),
                   'hint: {}'.format(e).format(self.aw, e))
        else:
            if r.status_code == 204:
                self.__showrequestandresponse(r, f'DELETE FILE {path}', showbody=False)
            else:
                sys.exit(f'Fatal: request failed ({r.status_code} {r.reason})')

    def close(self):
        """
        Close the connection to HCP Anywhere.
        """
        self.session.close()

    def __showrequestandresponse(self, r, title, showbody=True):
        """
        Print a Request and its Response nicely formatted.

        :param r:         a response object
        :param title:     a title to be used as heading
        :param showbody:  bool, if the body shall be shown
        """
        print(title)
        print('='*len(title))
        print('\tREQUEST:')
        print(f'\t\t{r.request.method} {r.request.url}')
        print('\t\tHeaders:')
        wanted = ['accept', 'content-type', 'x-hcpaw-fss.api-version', 'authorization', ]
        print('\t\t\t' + '\n\t\t\t'.join(
            [f'{x}: {r.request.headers[x]}' for x in r.request.headers if x.lower() in wanted]))
        print('\t\tBody:')
        _b = loads(r.request.body)
        if 'password' in _b:
            _b['password'] = '******'
        print(f'\t\t\t{_b}')

        print('\tRESPONSE:')
        print(f'\t\tHTTP code: {r.status_code} - {r.reason}')
        print('\t\tHeaders:')
        wanted = ['x-hcpaw-supported-fss-api-versions', 'x-hcpaw-fss-api-version', 'x-hcpaw-system-id_',
                  'x-hcpaw-fss-api-entry', 'content-disposition', 'content-type', 'content-length']
        print('\t\t\t' + '\n\t\t\t'.join([f'{x}: {r.headers[x]}' for x in r.headers if x.lower() in wanted]))
        if showbody:
            print('\t\tBody:')
            _b = loads(r.text)
            print('\t\t\t' + '\n\t\t\t'.join(pformat(_b).split('\n')))
        print()
        try:
            click.confirm("Continue?", default=True, abort=True)
        except click.exceptions.Abort:
            sys.exit('Canceled')
        print()
        print()
