# The MIT License (MIT)
#
# Copyright (c) 2022 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
from os import access, F_OK, makedirs
from json import load

import click


def loadconfig(configfile):
    """
    Load the configuration from a json file; if none exists, create a template.

    :param configfile:  the config file's name
    :return:            a dict
    """
    if not access(configfile, F_OK):
        try:
            createtemplate(configfile)
        except Exception as e:
            print(f'Creation of template config file "{configfile}" failed\n'
                  f'\thint: {e}')
            sys.exit()
        else:
            print(f'Creation of template config file "{configfile}" was successful'
                  f'\n\tYou need to edit it to fit your needs!')
            sys.exit()
    else:
        with open(configfile, 'r') as config:  # load the configuration from a json file
            cnf = load(config)
        return cnf


__template = \
"""
{
   "hcp anywhere":{
      "USER":"The HCP Anywhere user's name",
      "PASSWORD":"password, base64 encoded!!!",
      "URI":"https://hcpanywhere.company.com",
      "PATHS":[
         "/Folder/TeamFolder1",
         "/Folder/TeamFolder2"
      ]
   },
   "s3":{
      "ENDPOINT":"https://s3endpoint.company.com",
      "BUCKET":"awdata",
      "PATH":"HCPAW_Upload",
      "ACCESSKEY":"****",
      "SECUREKEY":"****"
   }
}
"""


def createtemplate(configfile):
    """
    Create a template configuration file.

    :param configfile:  the file's name
    """
    try:
        click.confirm('A configuration file is not available.\n'
                      'Do you want to create a template configuration file?',
                      default=False, abort=True)
    except click.exceptions.Abort:
        sys.exit('Fatal: user denied creation')
    else:
        with open(configfile, 'w') as outhdl:
            print(__template, file=outhdl)
