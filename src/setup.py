import sys
from setuptools import setup, find_packages
from codecs import open  # To use a consistent encoding
from os import path

sys.path.insert(0, path.abspath('hcpaw2s3lib'))
from init import Gvars


here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='hcpaw2s3',
    version=str(Gvars.s_version),
    description=Gvars.Description,
    long_description=long_description,
    url='https://hcptools.snomis.eu',
    author=Gvars.Author,
    author_email=Gvars.AuthorMail,
    license='MIT',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: System Administrators',
        'Topic :: System :: Archiving',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Natural Language :: English',
    ],
    keywords='Amazon S3 compatible HCP Anywhere S3',
    packages=find_packages(exclude=[]),
    install_requires=['boto3', 'requests', 'click'],
    entry_points={'console_scripts': ['hcpaw2s3 = hcpaw2s3lib.__main__:main']}
)
